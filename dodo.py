from doit.tools import CmdAction, LongRunning

DOIT_CONFIG = {
    "backend": "json",
    "dep_file": ".doit.json",
    "verbosity": 2,
    "default_tasks": ["sync", "yarn_install", "install_hooks"],
    "action_string_formatting": "new",
}


def task_migrate():
    """ migrate """

    def migrate(*args, pos_arg, **kwargs):
        return ["CALL", "python", "backend/manage.py", "migrate"] + pos_arg

    return {"actions": [CmdAction(migrate)], "pos_arg": "pos_arg"}


def task_makemigrations():
    """ makemigrations """

    def makemigrations(*args, pos_arg, **kwargs):
        return ["CALL", "python", "backend/manage.py", "makemigrations"] + pos_arg

    return {"actions": [LongRunning(makemigrations)], "pos_arg": "pos_arg"}


task_m = task_migrate
task_mm = task_makemigrations


