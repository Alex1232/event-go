from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField('Дата создания', auto_now_add=True, editable=False)

    class Meta:
        ordering = ['created_at']
        abstract = True