from rest_framework import serializers
from django.utils import formats


class BaseSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField()

    class Meta:
        ordering = ['created_at']
        abstract = True

    def get_created_at(self, obj):
        return formats.date_format(obj.created_at, 'DATETIME_FORMAT')