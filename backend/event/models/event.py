from django.contrib.auth.models import User
from django.db import models

from base.models.base_model import BaseModel
from event_go import settings


class Event(BaseModel):
    name = models.CharField(
        blank=False,
        null=False,
        max_length=settings.DATABASE_STRING_LENGTH,
        verbose_name='Наименование'
    )
    description = models.TextField(blank=False, null=False, verbose_name='Описание')
    poster = models.ImageField(blank=True, null=True, upload_to='event/poster/', verbose_name='Постер для мероприятия')
    city = models.CharField(
        blank=False,
        null=False,
        max_length=settings.DATABASE_STRING_LENGTH,
        verbose_name='Город, где проходит мероприятие'
    )
    author_event = models.ForeignKey(
        User,
        models.CASCADE,
        blank=False,
        null=False,
        verbose_name='Автор мероприятия',
    )
    like = models.PositiveIntegerField(blank=True,  null=True, verbose_name='Нравится')
    view = models.PositiveIntegerField(blank=True, null=True, verbose_name='Просмотры')
    maximum_number_participants = models.PositiveIntegerField(
        blank=False,
        null=False,
        verbose_name='Максимальное число участников'
    )
    participants = models.ManyToManyField(User, verbose_name='Участники', related_name='participants_user')

    class Meta:
        verbose_name = 'Мероприятие'
        verbose_name_plural = 'Мероприятия'
        ordering = ['-created_at']

    def __str__(self):
        return f'Мероприятие - {self.name}'


class ImagesEvent(BaseModel):
    image = models.ImageField(
        blank=True,
        null=True,
        upload_to='event/images/',
        verbose_name='Все картинки мероприятия'
    )
    event = models.ForeignKey(
        Event,
        models.CASCADE,
        blank=False,
        null=False,
        verbose_name='Мероприятие',
    )

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
        ordering = ['-created_at']

    def __str__(self):
        return f'Картинка мероприятия - {self.event}'


class Comment(BaseModel):
    username = models.ForeignKey(
        User,
        models.CASCADE,
        blank=False,
        null=False,
        verbose_name='Автор комментария',
        related_name="comment_author",
    )
    text = models.TextField(blank=False, null=False, verbose_name='Комментарий')
    parent_answered = models.ForeignKey(
        User,
        models.CASCADE,
        blank=True,
        null=True,
        verbose_name='Кому ответили',
        related_name="comment_addressed",
    )
    event = models.ForeignKey(
        Event,
        models.CASCADE,
        blank=False,
        null=False,
        verbose_name='К какому мероприятию принадлежит',
        related_name="comment_event",
    )

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ['-created_at']

    def __str__(self):
        return f'Комментарий, для - {self.event}'