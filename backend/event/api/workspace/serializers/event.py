from rest_framework import serializers

from base.api.workspace.serializers.base_serializers import BaseSerializer
from event.models.event import Event, ImagesEvent, Comment


class EventSerializer(BaseSerializer):
    author_event = serializers.SerializerMethodField()
    like = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = '__all__'

    def get_author_event(self, obj):
        return str(f"{obj.author_event.last_name} {obj.author_event.first_name}")

    def get_like(self, obj):
        if obj.like:
            return obj.like
        else:
            return '0'


class ImagesEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImagesEvent
        fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'