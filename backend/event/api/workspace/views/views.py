from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework import status

from event.api.workspace.serializers.event import EventSerializer, ImagesEventSerializer, CommentSerializer
from event.models import Event, ImagesEvent, Comment
from rest_framework import viewsets


class EventViewSet(viewsets.ModelViewSet):

    queryset = Event.objects.all()
    serializer_class = EventSerializer

    @action(methods=['GET'], detail=True, url_path='add_like')
    def add_like(self, request, *args, **kwargs):
        event = get_object_or_404(Event, id=kwargs['pk'])
        event.like += 1
        event.save()
        return Response(status=status.HTTP_200_OK, data={"like": event.like})


class ImagesEventViewSet(viewsets.ModelViewSet):

    queryset = ImagesEvent.objects.all()
    serializer_class = ImagesEventSerializer


class CommentViewSet(viewsets.ModelViewSet):

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer