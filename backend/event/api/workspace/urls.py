from rest_framework import routers

from . import views

app_name = 'event'

router = routers.DefaultRouter()

router.register(r'events', views.EventViewSet, 'event')
router.register(r'images-events', views.ImagesEventViewSet, 'image-event')
router.register(r'comments', views.CommentViewSet, 'comment')

urlpatterns = router.urls
