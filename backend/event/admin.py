from django.contrib import admin
from .models import Event, ImagesEvent, Comment

admin.site.register(Event)
admin.site.register(ImagesEvent)
admin.site.register(Comment)