# Generated by Django 3.2.4 on 2021-06-19 21:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0011_auto_20210619_2235'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='draft',
        ),
    ]
