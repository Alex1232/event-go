# Generated by Django 3.2.4 on 2021-06-19 18:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0008_auto_20210619_2032'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='like',
            field=models.PositiveIntegerField(blank=True, editable=False, null=True, verbose_name='Нравится'),
        ),
    ]
