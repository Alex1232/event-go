from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from event_go import settings

workspace_api_urls = [
    path('event/', include('event.api.workspace.urls', namespace='event')),
]


api_urls = [
    path('workspace/', include((workspace_api_urls, 'event_go'), namespace='workspace')),
]


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include((api_urls, 'event_go'), namespace='api')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
