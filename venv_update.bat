if not exist venv (
  ECHO "INIT..."
  CALL python -m venv venv || GOTO :exit
)
CALL venv\Scripts\activate || GOTO :exit

CALL python -m pip install --upgrade pip==21.0.1 pip-tools==5.5.0 doit==0.33.1 || GOTO :exit

CALL python -m pip install -r requirements.txt || GOTO :exit

CD frontend || GOTO :exit
CALL yarn install || GOTO :exit
CD .. || GOTO :exit