import { createRouter, createWebHistory } from 'vue-router'
import EventList from "@/app/workspace/event/components/EventList";
import EventDetail from "@/app/workspace/event/components/EventDetail";

const routes = [
    {
        path: '',
        component: EventList,
        name: 'event-list',
        props: route => ({apiPath: `http://localhost:8080/api/workspace/event/events/`, ...route.params}),
    },
    {
        path: '/event/:id',
        component:EventDetail,
        name: 'event-deteil',
        props: route => ({apiPath: `http://localhost:8080/api/workspace/event/events/${route.params.id}/`, ...route.params}),
    },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
